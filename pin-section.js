"use strict";

docReady(() => {

    const containers = document.querySelectorAll('.demoVideo-container, .demo3d-container, .image-container');
    const isMobile = window.innerWidth < 768;
    containers.forEach(element => {
        const text = element.querySelector('.demoVideo-text-container');
        if(isMobile && element.classList.contains('use-as-slider')){
            const tl_mobile = new TimelineMax();
            const pin = element.querySelector(".pin-img");
            tl_mobile.to(text, {background: "#fff", ease: Power1.easeOut},'first')
            tl_mobile.to(pin, {scale: '1.2', ease: Power1.easeOut},'first');

            ScrollTrigger.create({
                trigger: element,
                animation: tl_mobile,
                start: "top top",
                end: "bottom top+=50%",
                scrub: true,  
            })
        }
        else{
            const container = element.children[0]
            const spaceX = element.querySelectorAll('.space-left, .space-right');
            setSpaceWidth(spaceX, container.offsetLeft + 15);
            window.addEventListener('resize', () => {setSpaceWidth(spaceX, container.offsetLeft + 15)});

            const toggleClass = element.classList.contains('use-as-slider') ? 'scroll-mask-anim' : 'full-screen';
            const isScrub = element.classList.contains('use-as-slider');
            const tl_main = gsap.timeline();
            
            if(element.classList.contains('use-as-slider')){
                tl_main.to('.space-right, .space-left', 0.1, {width : '0px', ease: Power1.easeOut},'first')
                tl_main.to('.space-top, .space-bottom', 0.1, {height : '0px', ease: Power1.easeOut},'first');
            }
            
            ScrollTrigger.create({
                trigger: element,
                animation: tl_main,
                start: "top top",
                end: "bottom bottom",
                scrub: isScrub,
                toggleClass: toggleClass
            });
            
            if(text){
                const text_tl = gsap.timeline();
                const text_start = element.classList.contains('use-as-slider') ? 'top' : 'center';
                const text_trigger = element.classList.contains('use-as-slider') ? element : text;
                if(element.classList.contains('use-as-slider')){
                    text_tl
                        .to(text, 0.5, {autoAlpha: 0}, 'first')
                        .to('.use-as-slider h1', 1, {letterSpacing: 60, ease: Power1.easeOut}, 'first');
                }
                else{
                    text_tl
                        .to(text, 0.1, {opacity: 1})
                        .to(text, 0.1, {opacity: 1})
                        .to(text, 0.8, {opacity: 0});
                }
                
                ScrollTrigger.create({
                    trigger: text_trigger,
                    animation: text_tl,
                    start: "top" + text_start, 
                    end: "bottom top",
                    scrub: true,
                });

            }
        }
    });

    function setSpaceWidth(elements, width){
        elements.forEach(e => e.style.width = width + 'px');
    }

});
