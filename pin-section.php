<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$type = get_field('type');
$videoWebm = get_field('video_webm');
$videoOgg = get_field('video_ogg');
$videoMp4 = get_field('video_mp4');
$img = get_field('img');
$texte = get_field('texte');

$isSlider = get_field('isSlider') ? 'use-as-slider' : '';

?>

<section id="<?= $id ?>" class="<?= $type ?>-container position-relative <?= $isSlider ?> <?= $css ?>">
    <div class="container"></div>

    <?php if ($type == "demoVideo") : ?>
        <div class="position-sticky t-0">
            <?php if(!my_wp_is_mobile()): ?>
                <div class="<?php if( !get_field('isSlider') ){ ?>anim-500 <?php } ?> space-top"></div>
                <div class="<?php if( !get_field('isSlider') ){ ?>anim-500 <?php } ?> space-bottom"></div>
                <div class="<?php if( !get_field('isSlider') ){ ?>anim-500 <?php } ?> space-left"></div>
                <div class="<?php if( !get_field('isSlider') ){ ?>anim-500 <?php } ?> space-right"></div>
            <?php endif; ?>
            <?php if (my_wp_is_mobile()): 
                    $image = get_field('img');
                    if ( !empty($image) ): ?>

                        <?= acf_img(get_field('img'), 'medium', 'position-sticky t-0 l-0 pin-img', 'v0'); ?>
                    <?php endif; ?>
            <?php else: ?>
              <video autoplay loop muted id="v0" class="position-sticky t-0 l-0 pin-img" style="object-fit: cover;">
                <source type="video/webm" src="<?= $videoWebm ?>">
                </source>
                <source type="video/ogg" src="<?= $videoOgg ?>">
                </source>
                <source type="video/mp4" src="<?= $videoMp4 ?>">
                  <p>Sorry, your browser does not support the &lt;video&gt; element.</p>
                </video>
            <?php endif; ?>
        </div>
        <div class="position-sticky demoVideo-text-container">
            <div>
                <?= $texte ?>
            </div>
        </div>

    <?php elseif ($type == "image") : ?>

        <div class="position-sticky t-0">
            <?php if(!my_wp_is_mobile()): ?>
                <div class="anim-500 space-top"></div>
                <div class="anim-500 space-bottom"></div>
                <div class="anim-500 space-left"></div>
                <div class="anim-500 space-right"></div>
            <?php endif; ?>
            <?php
            $img = get_field('image_centrale');
            if ($img): ?>
              <img class="position-sticky t-0 l-0 pin-img v0" src="<?= $img['url']; ?>" alt="<?= $img['alt']; ?>" />
            <?php endif; ?>

        </div>
        <div class="position-sticky demoVideo-text-container">
            <div>
                <?= $texte ?>
            </div>
        </div>

    <?php endif; ?>



</section>
